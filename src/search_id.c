/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libspa
 * FILE NAME: search_id.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Linking of meshes either regular,
 * nested or irregular.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libspa Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* libspa includes the functions doing the spacial correspondances between the object of the program,
 * this library also include functions calculating the spacial water balance */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "spa_full.h"

int SPA_search_id(int type_struct, void *st, int id2find, int id_int_type, int id_out_type, FILE *fp) {
  int id;
  switch (type_struct) {

  case CPROD_SPA: {
    s_carac_fp *pchar_fp = (s_carac_fp *)st;
    id = FP_search_id_cprod(pchar_fp, id2find, id_int_type, id_out_type, fp);
    return id;
    break;
  }
  }
}