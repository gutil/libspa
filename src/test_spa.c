/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libspa
 * FILE NAME: test_spa.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Linking of meshes either regular,
 * nested or irregular.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libspa Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "reservoir.h"
#include "nonsat.h"
#include "production_functions.h"
#include "spa.h"

int main() {

  int nb_val;
  int id = 1;
  s_ids *ids;
  s_ids *idtmp;
  s_fprod *pfprod;
  s_fprod *pfprodtmp;
  double *val;
  // s_wat_bal *pwatbal;
  s_ids **corresp;
  corresp = (s_ids **)malloc(sizeof(s_ids *));
  *corresp = (s_ids *)malloc(sizeof(s_ids));
  // pwatbal=(s_wat_bal*)malloc(sizeof(s_wat_bal);

  nb_val = 4;
  ids = NULL;
  idtmp = NULL;
  pfprod = NULL;
  pfprodtmp = NULL;

  if (ids != NULL) {
    printf("erreur \n");
  } else {
    printf("nb_val=%d \n", nb_val);
    for (id = 1; id <= nb_val; id++) {
      idtmp = SPA_create_ids(id, 0.25);
      ids = SPA_secured_chain_id_fwd(ids, idtmp);
      if (ids->prev == NULL && id == 1) {
        printf("tout va bien\n");
      }
      if (ids->prev == NULL && id > 1) {
        printf("problème\n");
        break;
      } else
        printf("id : %d\n", ids->id);
    }
    id = 0;
    while (ids->prev != NULL) {
      if (id <= nb_val) {
        printf("id : %d\n", ids->id);
        ids = ids->prev;

        id++;
      } else {
        printf("problème boucle infinie\n");
        break;
      }
    }
    printf("id : %d\n", ids->id);
    printf("test\n");
    corresp = SPA_create_corresp_mesh(FPROD, ids);
    id = 0;
    while (corresp[FPROD]->next != NULL) {
      if (id < nb_val) {
        printf("id FP : %d\n", corresp[FPROD]->id);
        corresp[FPROD] = corresp[FPROD]->next;
        id++;
      } else {
        printf("problème boucle infinie\n");
        break;
      }
    }

    printf("id FP : %d\n", corresp[FPROD]->id);

    corresp[FPROD] = (s_ids *)SPA_browse_all_struct(IDS, corresp[FPROD], BEGINNING_TS);
    printf("id FP : %d\n", corresp[FPROD]->id);
    for (id = 1; id <= nb_val; id++) {

      pfprodtmp = FP_create_fprod(id);
      pfprodtmp->pwat_bal->q[FP_RUIS] = 10;
      pfprodtmp->pwat_bal->q[FP_INFILT] = 25;
      pfprod = FP_secured_chain_prod_fwd(pfprod, pfprodtmp);
    }

    val = SPA_calcul_values(FPROD, pfprod, corresp);
    printf("infilt_val = %f\n", val[FP_INFILT]);
    printf("ruis_val = %f\n", val[FP_RUIS]);
  }
}
