/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libspa
 * FILE NAME: struct_spa.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Linking of meshes either regular,
 * nested or irregular.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libspa Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* definition of spatial structure */
typedef struct id_spa s_id_spa;

/* chained IDs */
struct id_spa {
  /*!< id of the linked element*/
  int id;
  /*!< percentage of the spatial cover of the linked element over the element where the calcul is made*/
  double prct;
  /*!< pointer to the next element*/
  s_id_spa *next;
  /*!< pointer to the previous element*/
  s_id_spa *prev;
};

#define new_id_spa() ((s_id_spa *)malloc(sizeof(s_id_spa))) /*NF 31/12/03*/