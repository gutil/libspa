/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libspa
 * FILE NAME: manage_id_spa.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Linking of meshes either regular,
 * nested or irregular.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libspa Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "spa_full.h"

s_id_spa *chain_id(s_id_spa *id1, s_id_spa *id2) {
  id1->next = id2;
  id2->prev = id1;

  return id1;
}

s_id_spa *chain_id_fwd(s_id_spa *id1, s_id_spa *id2) {
  id1->next = id2;
  id2->prev = id1;

  return id2;
}

s_id_spa *SPA_secured_chain_id_fwd(s_id_spa *id1, s_id_spa *id2) {
  if (id1 != NULL) {
    id1 = chain_id_fwd(id1, id2);
  } else {
    id1 = id2;
  }

  return id1;
}

s_id_spa *SPA_secured_chain_id_bckwd(s_id_spa *id1, s_id_spa *id2) {
  if (id2 != NULL) {
    id2 = chain_id_fwd(id1, id2);
  } else {
    id2 = id1;
  }

  return id2;
}

s_id_spa *SPA_create_ids(int id, double prct) {
  s_id_spa *pids;
  pids = new_id_spa();
  bzero((char *)pids, sizeof(s_id_spa));
  pids->id = id;
  pids->prct = prct;

  return pids;
}

s_id_spa *SPA_create_id_void() {
  s_id_spa *pids;
  pids = new_id_spa();
  bzero((char *)pids, sizeof(s_id_spa));

  return pids;
}

s_id_spa *SPA_divide_ids(s_id_spa *ids, double area) {

  s_id_spa *id_tmp, *id_div, *id_div_tmp;
  id_div = NULL;

  id_tmp = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, ids, BEGINNING_SPA);

  while (id_tmp != NULL) {
    id_div_tmp = SPA_create_ids(id_tmp->id, id_tmp->prct / area);
    id_div = SPA_secured_chain_id_fwd(id_div, id_div_tmp);
    id_tmp = id_tmp->next;
  }
  id_div = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, id_div, BEGINNING_SPA);

  return id_div;
}

/** \fn s_ft *TS_free_ft(s_ft *pft)
 * \brief Deallocating a single _ft pointer
 */
s_id_spa *free_one_id(s_id_spa *one_id) {
  free(one_id);

  return NULL;
}

/** \fn s_ft *TS_free_ft(s_ft *pft)
 * \brief Deallocating a full time series
 */
s_id_spa *SPA_free_ids(s_id_spa *ids, FILE *fpout) {
  s_id_spa *id_tmp;
  // LP_printf(fpout,"Entering free_id_spa\n"); //BL to debug

  if (ids != NULL) {
    ids = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, ids, BEGINNING_SPA);

    id_tmp = ids;
    while (ids->next != NULL) {
      //  LP_printf(fp,"Deallocating %d\n",(int)pft->t);
      id_tmp = ids->next;
      // LP_printf(fpout,"freeing spa_id\n");// BL to debug
      ids = free_one_id(ids);
      ids = id_tmp;
    }

    // LP_printf(fpout,"freeing spa_id\n"); // BL to debug
    ids = free_one_id(ids);

    if (ids != NULL)
      LP_warning(fpout, "libts%4.2f: %s l%d _ft pointer not properly deallocated\n", VERSION_SPA, __FILE__, __LINE__);
  }

  return ids;
}

void SPA_print_ids(s_id_spa *ids, FILE *fpout) {
  s_id_spa *id_tmp;

  id_tmp = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, ids, BEGINNING_SPA);
  while (id_tmp != NULL) {
    LP_printf(fpout, "id %d prct %f \n", id_tmp->id, id_tmp->prct);
    id_tmp = id_tmp->next;
  }
}

s_id_spa *SPA_browse_id(s_id_spa *pid, int browse_type, FILE *flog) {
  s_id_spa *ptemp;

  if (pid != NULL)
    ptemp = pid;

  switch (browse_type) {
  case BEGINNING_SPA:
    while (ptemp->prev != NULL)
      ptemp = ptemp->prev;
    break;
  case END_SPA:
    while (ptemp->next != NULL)
      ptemp = ptemp->next;
    break;
  default:
    LP_error(flog, "libspa%4.2f, File %s in %s line %d : Unknown browsing direction.\n", VERSION_SPA, __FILE__, __func__, __LINE__);
    break;
  }

  return ptemp;
}