/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libspa
 * FILE NAME: functions_spa.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Linking of meshes either regular,
 * nested or irregular.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libspa Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// in manage_spa.c
void *SPA_browse_all_struct(int, void *, int);
double *SPA_calcul_values(int, void *, double, double, double, FILE *);
void SPA_correct_intersect(int, double, double, s_id_io *, void *, int, int, int, FILE *);
double *SPA_calcul_transport_fluxes(int, void *, int, double, double, double, FILE *);
double *SPA_dynamic_nonsat_values(int, void *, void *, double, double, int, FILE *);

// in manage_id_spa.c
s_id_spa *chain_id(s_id_spa *, s_id_spa *);
s_id_spa *SPA_secured_chain_id_fwd(s_id_spa *, s_id_spa *);
s_id_spa *SPA_secured_chain_id_bckwd(s_id_spa *, s_id_spa *);
s_id_spa *SPA_create_ids(int, double);
s_id_spa **SPA_create_link();
s_id_spa *SPA_divide_ids(s_id_spa *, double);
s_id_spa *free_one_id(s_id_spa *);
s_id_spa *SPA_free_ids(s_id_spa *, FILE *);
void SPA_print_ids(s_id_spa *, FILE *);
s_id_spa *SPA_create_id_void();
s_id_spa *SPA_browse_id(s_id_spa *, int, FILE *);

// in search_id.c
int SPA_search_id(int, void *, int, int, int, FILE *);
