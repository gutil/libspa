/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libspa
 * FILE NAME: param_spa.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Linking of meshes either regular,
 * nested or irregular.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libspa Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#define VERSION_SPA 0.11

enum type_struct { CPROD_SPA, CPROD_SOUT_SPA, BAL_UNIT_SPA, PARAM_FP_SPA, NSAT_SPA, MTO_SPA, IDS_SPA, RSV_SPA, NASH_SPA, STOCK_SPA, RIV_SPA, CPROD_GW_SPA, CPROD_NSAT_HDERM_SPA, NSAT_AQ_SPA, AQ_NSAT_HHYD_SPA, AQ_NSAT_SOIL_SPA, AQ_NSAT_CAUCHY_SPA, AQ_NSAT_TRVAR_SPA, N_TYPE_SPA };

#define BEGINNING_SPA BEGINNING_TS
#define END_SPA END_TS
#define EPS_SPA EPS_TS
#define EPS EPS_SPA
