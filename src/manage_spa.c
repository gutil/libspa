/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libspa
 * FILE NAME: manage_spa.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Linking of meshes either regular,
 * nested or irregular.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libspa Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* libspa includes the functions doing the spatial correspondences between the object of the
 * program, this library also include functions calculating the spacial water balance*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "spa_full.h"

void *SPA_browse_all_struct(int type_struct, void *st, int deb_fin) {
  int i = 1;
  switch (type_struct) {

  case CPROD_SPA: {
    s_cprod_fp *structurs = (s_cprod_fp *)st;
    if (deb_fin == BEGINNING_SPA) {
      while (structurs->prev != NULL) {
        structurs = structurs->prev;
      }
    } else {
      while (structurs->next != NULL) {

        structurs = structurs->next;
        i++;
      }
    }
    return structurs;
    break;
  }

  case BAL_UNIT_SPA: {
    s_bal_unit_fp *structurs = (s_bal_unit_fp *)st;
    if (deb_fin == BEGINNING_SPA) {
      while (structurs->prev != NULL) {
        structurs = structurs->prev;
      }
    } else {
      while (structurs->next != NULL) {
        structurs = structurs->next;
      }
    }
    return structurs;
    break;
  }

  case PARAM_FP_SPA: {
    s_param_fp *structurs = (s_param_fp *)st;
    if (deb_fin == BEGINNING_SPA) {
      while (structurs->prev != NULL) {
        structurs = structurs->prev;
      }
    } else {
      while (structurs->next != NULL) {
        structurs = structurs->next;
      }
    }
    return structurs;
    break;
  }

  case NSAT_SPA: {
    s_zns *structurs = (s_zns *)st;
    if (deb_fin == BEGINNING_SPA) {
      while (structurs->prev != NULL) {
        structurs = structurs->prev;
      }
    } else {
      while (structurs->next != NULL) {
        structurs = structurs->next;
      }
    }
    return structurs;
    break;
  }

  case NASH_SPA: {
    s_nash *structurs = (s_nash *)st;
    if (deb_fin == BEGINNING_SPA) {
      while (structurs->prev != NULL) {
        structurs = structurs->prev;
      }
    } else {
      while (structurs->next != NULL) {
        structurs = structurs->next;
      }
    }
    return structurs;
    break;
  }

  case RSV_SPA: {
    s_reservoir_rsv *structurs = (s_reservoir_rsv *)st;
    if (deb_fin == BEGINNING_SPA) {
      while (structurs->prev != NULL) {
        structurs = structurs->prev;
      }
    } else {
      while (structurs->next != NULL) {
        structurs = structurs->next;
      }
    }
    return structurs;
    break;
  }

  case MTO_SPA: {
    s_mto_fp *structurs = (s_mto_fp *)st;
    if (deb_fin == BEGINNING_SPA) {
      while (structurs->prev != NULL) {
        structurs = structurs->prev;
      }
    } else {
      while (structurs->next != NULL) {
        structurs = structurs->next;
      }
    }
    return structurs;
    break;
  }

  case IDS_SPA: {
    s_id_spa *structurs = (s_id_spa *)st;
    if (deb_fin == BEGINNING_SPA) {
      while (structurs->prev != NULL) {
        structurs = structurs->prev;
      }
    } else {
      while (structurs->next != NULL) {
        structurs = structurs->next;
      }
    }
    return structurs;
    break;
  }
  }
}

double *SPA_calcul_values(int type_struct, void *input, double prct, double t, double dt, FILE *fpout) {

  switch (type_struct) {

  case CPROD_SPA: {
    int i;
    double *output_fp;
    output_fp = (double *)malloc(RSV_DIRECT * sizeof(double));
    s_bal_unit_fp *pBU = (s_bal_unit_fp *)input;
    s_wat_bal_rsv *pwat_bal;
    char *name;
    pwat_bal = pBU->pwat_bal;
    for (i = 0; i < RSV_DIRECT; i++) {
      // name=RSV_name_res(i);// BL to debug
      output_fp[i] = pwat_bal->q[i] * (prct);
      // LP_printf(fpout,"%s %f %f \n",name,pwat_bal->q[i],output_fp[i]); //BL to debug
      // free(name); //BL to debug
    }
    return output_fp;
    break;
  }
    // NG : 20/02/2020 : New case CPROD_NSAT_HDERM_SPA added for hderm module
  case CPROD_NSAT_HDERM_SPA: {
    double *output_hderm;
    output_hderm = (double *)malloc(sizeof(double));
    s_zns *pzns = (s_zns *)input;
    s_wat_bal_rsv *wat_bal;
    wat_bal = pzns->wat_bal;
    output_hderm[0] = wat_bal->q[RSV_SOUT] * (prct);
    return output_hderm;
    break;
  }

  case CPROD_SOUT_SPA: {
    int i;
    double *output_fp;
    output_fp = (double *)malloc(2 * sizeof(double));
    s_bal_unit_fp *pBU = (s_bal_unit_fp *)input;
    s_wat_bal_rsv *pwat_bal;
    double *stock;

    stock = pBU->stock;
    pwat_bal = pBU->pwat_bal;
    output_fp[0] = pwat_bal->q[RSV_INFILT] * (prct);
    output_fp[1] = stock[FP_INFILT_STCK] * prct;
    return output_fp;
    break;
  }

  case STOCK_SPA: {
    int i;
    double *output_fp;
    output_fp = (double *)malloc(FP_NSTCK * sizeof(double));
    s_bal_unit_fp *pBU = (s_bal_unit_fp *)input;
    double *stock;

    stock = pBU->stock;
    for (i = 0; i < FP_NSTCK; i++) {
      output_fp[i] = stock[i] * (prct);
    }
    return output_fp;
    break;
  }

  case NSAT_SPA: {
    double *output_nsat;
    s_zns *pzns;
    pzns = (s_zns *)input;
    *output_nsat += pzns->wat_bal->q[RSV_SOUT] * (prct);
    return output_nsat;
    break;
  }

  case MTO_SPA: {
    double *output_mto;
    s_mto_fp *pmto;
    int nb_val, i;
    pmto = (s_mto_fp *)input;
    output_mto = FP_get_mto(t, pmto, fpout);
    // output_mto=FP_calc_mean_mto(t,t+dt_bassin,pmto,fpout);
    for (i = 0; i < FP_NMETEO; i++) {
      output_mto[i] *= prct;
    }
    return output_mto;
    break;
  }

  case RIV_SPA: {
    double *output_ruis;
    s_cprod_fp *cprod;
    cprod = (s_cprod_fp *)input;
    output_ruis = (double *)malloc(sizeof(double));
    output_ruis[0] = cprod->pwat_bal->q[RSV_RUIS] * prct;    // BL ruissellement
    output_ruis[0] += cprod->pwat_bal->q[RSV_SOUT] * prct;   // BL venant du souterrain.
    output_ruis[0] += cprod->pwat_bal->q[RSV_DIRECT] * prct; // BL venant du souterrain si pas d'aquifères
    return output_ruis;
    break;
  }

  case CPROD_GW_SPA: {
    double *output_surf;
    s_ele_msh *pele;
    s_face_msh *pface;
    double theta;
    theta = t;
    pele = (s_ele_msh *)input;
    output_surf = (double *)malloc(sizeof(double));
    pface = pele->face[Z_MSH][TWO];

    output_surf[0] = prct * (pface->phydro->pcond->conductance * ((1 - theta) * (pele->phydro->h[T] - pele->phydro->pbound->pft->ft) + (theta) * (pele->phydro->h[PIC] - pele->phydro->pbound->pft->ft)));
    // BL car dans boucle de picard l'update de la charge calculée est enregistrée dans pic.
    // output_surf[0]=prct*(pface->phydro->pcond->conductance/pface->p_descr[ONE]->surf)*(pele->phydro->h[PIC]-pele->phydro->pbound->pft->ft);

    return output_surf;
    break;
  }
  }
}

/**
 * \fn SPA_calcul_transport_fluxes(int,void*, int, double,double, double, FILE*)
 * \brief Manages transport fluxes calculations when transferred from one topology (geometry grid) to one other
 * \return table of values associated with the new grid
 */
double *SPA_calcul_transport_fluxes(int type_struct, void *input, int id_species, double prct, double t, double dt, FILE *fpout) {

  switch (type_struct) {
  case CPROD_SPA: // Passage du bilan ele_bu -> cprod
  {
    double *output_cprod_fluxes;
    output_cprod_fluxes = (double *)malloc(FP_FOUT_TYPE * sizeof(double));
    s_bal_unit_fp *pbu = (s_bal_unit_fp *)input;
    s_wat_bal_rsv *pwat_bal;
    pwat_bal = pbu->pwat_bal;
    output_cprod_fluxes[FP_MINF] = pwat_bal->trflux[id_species][RSV_INFILT] * prct;
    output_cprod_fluxes[FP_MRUIS] = pwat_bal->trflux[id_species][RSV_RUIS] * prct;

    return output_cprod_fluxes;
    break;
  }

  case STOCK_SPA: // Calcul des différents types de flux entrants pour le bilan ele_bu-> cprod
  {
    double *input_cprod_fluxes;
    input_cprod_fluxes = (double *)malloc(FP_FIN_TYPE * sizeof(double));
    s_bal_unit_fp *pbu = (s_bal_unit_fp *)input;
    input_cprod_fluxes[FP_MFORCED] = pbu->input_mflux[id_species][FP_MFORCED] * prct; // flux forcé
    input_cprod_fluxes[FP_MBOUND] = pbu->input_mflux[id_species][FP_MBOUND] * prct;   // flux bound

    return input_cprod_fluxes;
    break;
  }

  case CPROD_SOUT_SPA: // Reroutage d'une portion de flux infiltré en ruissellement réservoir RSV_DIRECT)
  {
    double *output;
    output = (double *)malloc(sizeof(double));
    s_wat_bal_rsv *pwat_bal;
    s_bal_unit_fp *pbu = (s_bal_unit_fp *)input;
    pwat_bal = pbu->pwat_bal;
    output[FP_MINF] = pwat_bal->trflux[id_species][RSV_INFILT] * (prct);

    return output;
    break;
  }

  case CPROD_NSAT_HDERM_SPA: // Calcul des apports au réseau hyperdermique depuis le RSV_SOUT du NSAT
  {
    double *output;
    output = (double *)malloc(sizeof(double));
    s_wat_bal_rsv *pwat_bal;
    s_zns *pzns = (s_zns *)input;
    pwat_bal = pzns->wat_bal;
    output[0] = pwat_bal->trflux[id_species][RSV_SOUT] * (prct);

    return output;
    break;
  }

  case RIV_SPA: // calcul du flux latéral total apporté en couplé
  {
    double *output = (double *)malloc(sizeof(double));
    s_cprod_fp *cprod;
    cprod = (s_cprod_fp *)input;
    output[0] = cprod->pwat_bal->trflux[id_species][RSV_RUIS] * prct;    // flux en runoff
    output[0] += cprod->pwat_bal->trflux[id_species][RSV_SOUT] * prct;   // flux depuis le souterrain
    output[0] += cprod->pwat_bal->trflux[id_species][RSV_DIRECT] * prct; // flux infiltré rerouté en runoff si pas de souterrain

    return output;
    break;
  }

  case NSAT_AQ_SPA: // Calcul des apports au souterrain depuis le RSV_SOUT du NSAT
  {
    double *output;
    output = (double *)malloc(sizeof(double));
    s_wat_bal_rsv *pwat_bal;
    s_zns *pzns = (s_zns *)input;
    pwat_bal = pzns->wat_bal;
    // output[0] = pwat_bal->trflux[id_species][RSV_INFILT] * (prct); // NG : Bug fix 12/04/2023
    output[0] = pwat_bal->trflux[id_species][RSV_SOUT] * (prct);
    return output;
    break;
  }
  }
}

void SPA_correct_intersect(int type_struct, double check, double val, s_id_io *to_correct, void *input_1, int length, int link, int id2check, FILE *fp) {

  switch (type_struct) {

  case CPROD_SPA: {
    s_carac_fp *pchar_fp;
    s_id_io *pid_tmp;
    s_cprod_fp *pcprod;
    double error, correct;
    int id_cp;

    pchar_fp = (s_carac_fp *)input_1;
    pid_tmp = IO_browse_id(to_correct, BEGINNING_IO);
    correct = val / check;

    if (isnan(correct) || isinf(correct)) {
      LP_error(fp, "error correction factur is Nan or Inf");
    }

    while (pid_tmp != NULL) {
      id_cp = pid_tmp->id;
      pcprod = pchar_fp->p_cprod[id_cp];
      pcprod->link[link] = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pcprod->link[link], BEGINNING_SPA);
      while (pcprod->link[link]->next != NULL) {
        if (pcprod->link[link]->id == id2check) {
          break;
        }
        pcprod->link[link] = pcprod->link[link]->next;
      }
      if (pcprod->link[link]->id == id2check) {
        pcprod->link[link]->prct *= correct;
      } else {
        LP_error(fp, "No id BU %d for id_cprod %d\n", id2check, id_cp);
      }
      if ((pcprod->link[link]->prct < 0 || isnan(pcprod->link[link]->prct)) || isinf(pcprod->link[link]->prct)) {
        LP_printf(fp, "prct %f  area_bu %f check %f error %f correcting factor %f\n", pcprod->link[link]->prct, val, check, correct);
        LP_error(fp, "During automatic surface correction check your input files for link between WBU %d and cprod %d \n", id2check + 1, pcprod->id[FP_GIS]);
      }
      pid_tmp = pid_tmp->next;
    }
    break;
  }

  case NSAT_SPA: {
    s_carac_zns *pchar_zns;
    s_id_io *pid_tmp;
    s_zns *pzns;
    double error, correct, prct;
    int id_zns;

    pchar_zns = (s_carac_zns *)input_1;
    pid_tmp = IO_browse_id(to_correct, BEGINNING_IO);
    correct = val / check;

    if (isnan(correct) || isinf(correct)) {
      LP_error(fp, "error correction factur is Nan or Inf");
    }
    while (pid_tmp != NULL) {
      id_zns = pid_tmp->id;
      pzns = pchar_zns->p_zns[id_zns];
      pzns->link[link] = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pzns->link[link], BEGINNING_SPA);
      while (pzns->link[link]->next != NULL) {
        if (pzns->link[link]->id == id2check) {
          break;
        }
        pzns->link[link] = pzns->link[link]->next;
      }
      if (pzns->link[link]->id == id2check) {
        prct = pzns->link[link]->prct;
        pzns->link[link]->prct *= correct;

      } else {
        LP_error(fp, "No id BU %d for id_zns %d\n", id2check, id_zns);
      }
      if ((pzns->link[link]->prct < 0 || isnan(pzns->link[link]->prct)) || isinf(pzns->link[link]->prct)) {
        LP_printf(fp, "prct %e area_bu %f check %f area_zns %f correcting factor %f\n", prct, val, check, pzns->area, correct);
        LP_error(fp, "During automatic surface correction check your input files for link between WBU %d and nsat %d %d\n", id2check + 1, pzns->id[NS_GIS] + 1, pid_tmp->id + 1);
      }
      pid_tmp = pid_tmp->next;
    }
    break;
  }

  case CPROD_GW_SPA: {
    s_carac_aq *pchar_aq;
    s_id_io *pid_tmp;
    s_ele_msh *pele;
    double error, correct;
    int id, id_lay;
    pchar_aq = (s_carac_aq *)input_1;
    pid_tmp = IO_browse_id(to_correct, BEGINNING_IO);
    correct = val / check;

    if (isnan(correct) || isinf(correct)) {
      LP_error(fp, "Error : correction factor is Nan or Inf");
    }

    while (pid_tmp != NULL) {
      id = pid_tmp->id;
      id_lay = pid_tmp->id_lay;
      pele = pchar_aq->pcmsh->p_layer[id_lay]->p_ele[id];
      pele->link[link] = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pele->link[link], BEGINNING_SPA);
      while (pele->link[link]->next != NULL) {
        if (pele->link[link]->id == id2check) {
          break;
        }
        pele->link[link] = pele->link[link]->next;
      }
      if (pele->link[link]->id == id2check) {
        pele->link[link]->prct *= correct;
      } else {
        LP_error(fp, "No id cprod %d for id_gw %d\n", id2check, pele->id[ABS_MSH]);
      }
      if ((pele->link[link]->prct < 0 || isnan(pele->link[link]->prct)) || isinf(pele->link[link]->prct)) {
        LP_printf(fp, "prct %f area_cp %f check %f correcting factor\n", pele->link[link]->prct, val, check, correct);
        LP_error(fp, "During automatic surface correction check your input files for link between cprod %d and id_gw %d %d\n", id2check + 1, pele->id[ABS_MSH], id);
      }
      pid_tmp = pid_tmp->next;
    }
    break;
  }
  }
}

double *SPA_dynamic_nonsat_values(int type_struct, void *input_a, void *input_b, double prct, double t, int id_species, FILE *fpout) {

  switch (type_struct) {

  case AQ_NSAT_HHYD_SPA: {
    double *values;
    s_ele_msh *pele = (s_ele_msh *)input_a;
    s_zns *pzns = (s_zns *)input_b;
    values = (double *)calloc(2, sizeof(double));

    values[0] = (pele->phydro->h[ITER] * pele->pdescr->surf * prct) / pzns->area;
    values[1] = pele->pdescr->surf * prct / pzns->area;
    return values;
    break;
  }

  case AQ_NSAT_SOIL_SPA: {
    double *values;
    s_ele_msh *pele = (s_ele_msh *)input_a;
    s_zns *pzns = (s_zns *)input_b;
    values = (double *)calloc(2, sizeof(double));
    values[0] = (pele->phydro->pbound->pft->ft * pele->pdescr->surf * prct) / pzns->area;
    values[1] = (pele->pdescr->surf * prct) / pzns->area;
    return values;
    break;
  }

  case AQ_NSAT_CAUCHY_SPA: {
    double *values;
    s_ele_msh *pele = (s_ele_msh *)input_a;
    s_zns *pzns = (s_zns *)input_b;
    values = (double *)calloc(2, sizeof(double));
    s_ft *pft;
    double val;
    pft = TS_function_t_pointed(t, pele->phydro->pbound->pft, fpout);
    val = TS_function_value_t(t, pft, fpout);
    values[0] = (val * pele->pdescr->surf * prct) / pzns->area;
    values[1] = (pele->pdescr->surf * prct) / pzns->area;
    return values;
    break;
  }

  case AQ_NSAT_TRVAR_SPA: {
    double *values;
    s_ele_msh *pele = (s_ele_msh *)input_a;
    s_zns *pzns = (s_zns *)input_b;
    values = (double *)calloc(2, sizeof(double));

    values[0] = (pele->phydro->transp_var[id_species] * pele->pdescr->surf * prct) / pzns->area;
    values[1] = pele->pdescr->surf * prct / pzns->area;
    return values;
    break;
  }

  default:
    LP_error(fpout, "In libspa%4.2f : In file %s, function %s, at line %d : Unknown case.\n", VERSION_SPA, __FILE__, __func__, __LINE__);
  }
}
